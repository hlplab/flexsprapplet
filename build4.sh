export flexlib='/usr/local/flex_sdk_4.1'
${flexlib}/bin/mxmlc -creator "Hal Tily and Andrew Watts" -title "allapp" -localized-description "Self Paced Reading Applet" en-US -compiler.debug -theme=${flexlib}/frameworks/themes/Halo/halo.swc flexspr_4.mxml -output=flexspr_4.1.swf
